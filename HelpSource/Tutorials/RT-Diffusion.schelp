title:: Real-Time Diffusion
summary:: DXARTS 463
categories:: Tutorials>DXARTS>463
keyword:: DXARTS

table::
## strong::TOC:: || link::Tutorials/DXARTS-463-TOC::
## strong::Previous:: || link::Tutorials/RT::
## strong::Next:: || link::Tutorials/ATK::
::

section:: Live Diffusion

Spring 2018's DXARTS concert gave us a glimpse into the world of live diffusion. In that concert, Dr. Anderson diffused Bernard Parmegiani's De Nartura Sonorum. A common workflow for diffuising a stereo piece, is to route the left channel to all speakers on the left half and route the right channel to all speaker on the right half. Using faders on a mixing board, we control which speakers are playing sound and thus we control where the sound is in space (mostly front-back or up-down, since the stereo file contains the left-right information already).

It is recommended that you make a diffusion score. This is using the template that we have been using to make our graphic score. On the diffusion template we can mark important moments in the piece and what should happen spatially. A common technique that Dr. Anderson uses is to mark of each section on the score, and each section starts "on stage", or front and center. As the section progress, and more sounds start to happen, we can start to bring the sounds off stage and into and around the concert hall.

Depending on your speaker setup, there may be what we call diffuse or far-field speakers. These are generally farther away from the general array or are pointed away from the audience into the walls and rafters of the concert hall. Rather than providing direct point source sounds, these add the general ambience or diffuse field.

Since we do not have regular access to a concert hall equipped with a speaker array, we will try to practice using ambisonics. We will create a number of virtual speakers that we will then encode to ambisonics so we can audition and practice our diffusion using an ambisonic speaker array (Raitt 205, 117 or 113). For now we will use the midi controller so we can practice using faders. In week 8, we will look at using other controllers, such as TouchOSC to control to ambisonic field.

subsection:: Virtual Speakers

We need to setup an encoder to get our virtual speakers in B-Format. We can use the link::Classes/FoaEncoderMatrix#*newPeri:: method to setup a 3D set of speakers. We need to make sure we know the order of the speakers so we can send the correct signals to each speaker. We can get some information from our encoder using link::Classes/FoaEncoderMatrix#-dirChannels#-dirChannels:: to view the array of speaker angles.

code::
(
e = FoaEncoderMatrix.newPeri;
e.dirChannels.raddeg
)
::

We can see that we have a list of eight speakers, the first four the upper quad "ring" and the last 4 the lower quad "ring". And we can see that our speakers are teletype::[left, left, right, right, left, left, right right]::.

Let's load a stereo file and encode it into ambisonics, using our virtual speakers.

code::
(
var synth, decoder, buffer;

s.waitForBoot({

	buffer = CtkBuffer("/Users/dan/Desktop/Sud_ All.wav").load;

	decoder = FoaDecoderKernel.newUHJ;

	s.sync;

	synth = CtkSynthDef('rtDiffusion', {
		arg buffer;
		var sig, virtualSig, encode, decode;
		sig = PlayBuf.ar(2, buffer, BufRateScale.kr(buffer));
		virtualSig = [sig[0], sig[0], sig[1], sig[1], sig[0], sig[0], sig[1], sig[1]];
		encode = FoaEncode.ar(virtualSig, FoaEncoderMatrix.newPeri);
		decode = FoaDecode.ar(encode, decoder);
		Out.ar(0, decode)
	});

	synth.note(0.0, buffer.duration).buffer_(buffer).play;
})
)

::

subsection:: Setting up the MIDI controller

Now we need to setup our MIDI controller to control each speaker individually. I am going to set it up so that the middle two faders are front left and right respectively. The next two will be back left and right, then upper front left and right, then upper back left and right. So all the faders to the left of middle are left speakers and all the faders to the right of middle are right speakers.

Let's setup some test code to begin with.

code::
(
var midiFunc;

MIDIIn.connectAll;

midiFunc = MIDIFunc.new({arg val, num, chan, src;
	case
	{num == 0} {("Back Left Up Speaker is: " ++ val).postln}
	{num == 1} {("Front Left Up Speaker is: " ++ val).postln}
	{num == 2} {("Back Left Down Speaker is: " ++ val).postln}
	{num == 3} {("Front Left Down Speaker is: " ++ val).postln}
	{num == 4} {("Front Right Down Speaker is: " ++ val).postln}
	{num == 5} {("Back Right Down Speaker is: " ++ val).postln}
	{num == 6} {("Front Right Up Speaker is: " ++ val).postln}
	{num == 7} {("Back Right Up Speaker is: " ++ val).postln}
}, msgType: 'control');

)
::

Let's think about which speaker is which now in our encoder array. To use the ambisonics convention, we will use, for instance "BLU" for back left up, and so on.

table::
## BLU || Fader 0 || Encoder Array 1
## FLU || Fader 1 || Encoder Array 0
## BLD || Fader 2 || Encoder Array 5
## FLD || Fader 3 || Encoder Array 4
## FRD || Fader 4 || Encoder Array 7
## BRD || Fader 5 || Encoder Array 6
## FRU || Fader 6 || Encoder Array 3
## BRU || Fader 7 || Encoder Array 2
::

subsection:: Putting it all together

Now that we have everything in place, let's put it all together.

code::
(
var synth, decoder, buffer, note;
var midiFunc, controlSpec;

MIDIIn.connectAll;

controlSpec = ControlSpec(-inf, 0, 'db');

s.waitForBoot({

	buffer = CtkBuffer("/Users/dan/Desktop/Sud_ All.wav").load;

	decoder = FoaDecoderKernel.newUHJ;

	s.sync;

	synth = CtkSynthDef('rtDiffusion', {
		arg buffer, amp0 = -inf, amp1 = -inf, amp2 = -inf, amp3 = -inf, amp4 = -inf, amp5 = -inf, amp6 = -inf, amp7 = -inf;
		var sig, virtualSig, encode, decode, amp;
		amp = [amp0, amp1, amp2, amp3, amp4, amp5, amp6, amp7];
		sig = PlayBuf.ar(2, buffer, BufRateScale.kr(buffer));
		virtualSig = [sig[0], sig[0], sig[1], sig[1], sig[0], sig[0], sig[1], sig[1]] * amp.dbamp;
		encode = FoaEncode.ar(virtualSig, FoaEncoderMatrix.newPeri);
		decode = FoaDecode.ar(encode, decoder);
		Out.ar(0, decode)
	});

	midiFunc = MIDIFunc.new({arg val, num, chan, src;
		val = controlSpec.map(val.linlin(0, 127, 0, 1));
		case
		{num == 0} {("Back Left Up Speaker is: " ++ val).postln; note.amp1_(val)}
		{num == 1} {("Front Left Up Speaker is: " ++ val).postln; note.amp0_(val)}
		{num == 2} {("Back Left Down Speaker is: " ++ val).postln; note.amp5_(val)}
		{num == 3} {("Front Left Down Speaker is: " ++ val).postln; note.amp4_(val)}
		{num == 4} {("Front Right Down Speaker is: " ++ val).postln; note.amp7_(val)}
		{num == 5} {("Back Right Down Speaker is: " ++ val).postln; note.amp6_(val)}
		{num == 6} {("Front Right Up Speaker is: " ++ val).postln; note.amp3_(val)}
		{num == 7} {("Back Right Up Speaker is: " ++ val).postln; note.amp2_(val)}
	}, msgType: 'control');

	note = synth.note(0.0, buffer.duration).buffer_(buffer).play;
})
)

::

Note:: When you are in one of the labs connected to the speaker system, you do not need to decode, so you should send out the 4-channel B-format signal.::

section:: Credits

Daniel Peterson, University of Washington, 2018
