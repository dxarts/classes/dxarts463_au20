title:: Scheduling
summary:: DXARTS 463
categories:: Tutorials>DXARTS>463
keyword:: DXARTS

table::
## strong::TOC:: || link::Tutorials/DXARTS-463-TOC::
## strong::Previous:: || link::Tutorials/SuperCollider-Review::
## strong::Next:: || link::Tutorials/Real-Time-Sound::
::

section:: Real Time Server

Before we take a look at scheduling, we should prepare our server, which will allow us to synthesize sound in real-time.

section:: ServerOptions

Before booting the server, we may wish to set some parameters. We can use link::Classes/ServerOptions:: to see what we can change on the server we want to use.

code::
o = ServerOptions.new;
::
Note:: The options MUST be set before the server is booted, as these options are not changeable once a server is booted.::

subsection:: Devices

ServerOptions can help us choose the sound device we want to use. Alternately we can set the device we want to use (on macOS) using Audio MIDI Setup.

Note:: If you are on Windows, I will do my best to help, but I don't have have good experiences using different devices on Windows.::

code::
ServerOptions.devices;
::

subsection:: Outputs

Depending on our hardware, we may want to change from the default 8 output channels.

code::
// set to 16 output channels
o.numOutputBusChannels_(16);
::

subsection:: Inputs

We can also set the number of inputs.

code::
o.numInputBusChannels_(16)
::

subsection:: Memory

Depending on what memory allocation we need, e.g. when using Delays, we can up the amount of memory available to SuperCollider.

code::
o.memSize_(2**21);
::

section:: Server

Now we are ready to boot our link::Classes/Server::. We can do this in a few ways. Typically, the variable s is reserved for the server.

code::
s = Server.default;
s.options_(o);
s.boot;
::

subsection:: WaitForBoot

Often we may have a reason to wait for the server to boot before we run code. This is important in most real-time instances as we will be starting synths, allocating buffers, etc. and we need the server to be booted before the code is run. We can use link::Classes/Server#-waitForBoot:: to achieve this. The method takes a function as its first argument. This function is evaluated after the server has been booted successfully. We can wrap our entire code project in this function if we would like. The waitForBoot method actually creates a link::Classes/Routine:: which we will see below, so we can use scheduling methods within the waitForBoot such as .wait.

code::
(
s = Server.default;
s.options_(ServerOptions.new.numOutputBusChannels_(16).memSize_(2**21));
s.waitForBoot({
	"Server Booted! And we waited!!".postln;
});
)
::

subsection:: KillAll

Sometimes phantom servers can stick around running in the background, disconnected from the interpreter. This often results in the code::Exception in World_OpenUDP: unable to bind udp socket:: erro message. We can send a message to the OS to kill all scsynth servers. This code could be handy to have in comments at the end of your project in case you can't boot the server

code::
Server.killAll;
::

subsection:: PlotTree

It will be useful from time to time to inspect the server. We can get messages from the server using OSC, but we will look at that in a few weeks. For now we can inspect the node tree. This is a GUI that will show us in real-time what groups and nodes are playing on the server.

code::
s.plotTree
::

subsection:: NewAllocators

The server allocates all numbers for you for nodes, groups, buffers, etc. When running your code over and over with the same server running, these numbers, especially nodes can continue to go up and up until you hit the code::maxNodes:: even though most all have been released. Then there won't be any sound because those node numbers don't exist. We can used the message link::Classes/Server#-newAllocators:: to reset these numbers as if the server was freshly booted.

code::
s.newAllocators
::

This may be handy to have at the very top of your project. It can never hurt unless you are still need to use something that has been allocated.


section:: Real-Time Sound

There are many ways in SuperCollider to make sound. Typically my examples will mostly use the Ctk. But we will review the different approaches here and you can work with whatever you feel comfortable with.

subsection:: Sending messages to the Server

This is how I learned to use SuperCollider and if you are mostly familiar with the NRT score based approach and have ever inspected the score, this may look familiar. We can send messages directly to the server. This approach allows for the most customization, but is rarely used currently. You will have to do book keeping yourself. I won't spend a lot of time on this, but if you want to know this we can look at this in lab or during office hours.

code::
// first we should boot our server if not already booted
s = Server.default;
s.options_(ServerOptions.new.numOutputBusChannels_(16).memSize_(2**21));
s.boot;

(
// note we need to add to synth to the server
SynthDef('sinosc', {
	arg freq = 440;
	Out.ar(0, SinOsc.ar(freq))
}).add;
)

// play a note
s.sendMsg('/s_new', 'sinosc', 1001, 0, 1, 'freq', 220)
// free the note
s.sendMsg('/n_free', 1001)

// play a note
s.sendMsg('/s_new', 'sinosc', 1001, 0, 1, 'freq', 220)
// change frequency
s.sendMsg('/n_set', 1001, 'freq', 440)
// free the note
s.sendMsg('/n_free', 1001)
::

subsection:: Synth

This is the method some of you may choose. Here we use link::Classes/Synth:: to play notes.

code::
(
// note we need to add to synth to the server
SynthDef('sinosc', {
	arg freq = 440;
	Out.ar(0, SinOsc.ar(freq))
}).add;
)

// create and play a note
x = Synth.new('sinosc', ['freq', 220])
// set an argument of the node
x.set('freq', 110)
// free the node
x.free
::

subsection:: Ctk

Most of you are very familiar with Ctk. I feel that it offers some functionality that we will see throughout the quarter that makes it the easiest to use (but sometimes the hardest to customize). I am fine with whatever syntax you choose to use.

code::
(
// note we no longer need to add to the server as Ctk handles this for us
d = CtkSynthDef('sinosc', {
	arg freq = 440;
	Out.ar(0, SinOsc.ar(freq))
});
)

// create and play a note
x = d.note.freq_(220).play;
// set an argument of the node
x.freq_(440);
x.freq = 440
// free the node
x.free;
::

section:: Freeing Synths

We will take a moment to look at options for cleanly freeing synths.

subsection:: doneAction

Some UGens have a link::Classes/Done#Actions#doneAction::. Let's take a look at a few.

code::

// choose a random buffer
b = CtkBuffer((Platform.resourceDir ++ "/sounds/*").pathMatch.choose).load;

(
// playbuf with a doneAction
d = CtkSynthDef('playbuf', {
	arg buffer;
	Out.ar(0, PlayBuf.ar(1, buffer, BufRateScale.kr(buffer), doneAction: 2))
});
)

// play the note, the node will automatically be freed by doneAction
x = d.note.buffer_(b).play

b.free;

::

We can see that with a doneAction of 2, the synth is freed when the buffer is finished playing.

It may make more sense to have the synth play for an indetermined amount of time. To do this we can use EnvGen to hold the synth open and then free it. We will use the release node in Env. We can use the gate argument to hold the Env on the node immediately preceding the release node and then finally release it.

code::

// choose a random buffer
b = CtkBuffer((Platform.resourceDir ++ "/sounds/*").pathMatch.choose).load;

(
// EnvGen with a gate and doneAction
d = CtkSynthDef('playbuf', {
	arg buffer, gate = 1; // set the gate to 1 to hold the Env open
	var env;
	env = EnvGen.kr(Env([0, 1, 1, 0], [0.1, 1.0, 2.0], 'sin', 2), gate, doneAction: 2);
	Out.ar(0, PlayBuf.ar(1, buffer, BufRateScale.kr(buffer), loop: 1) * env)
});
)

// play the note
x = d.note.buffer_(b).play

// set the gate to 0 and fade the synth out, then free it
x.gate_(0)

// alternately, Ctk has a built in method that will set the gate to 0, but will not free the synth without the doneAction
x.release;

// free the buffer
b.free;
::

section:: Scheduling

While using Ctk we can schedule sound for the future...

code::

// choose a random buffer
b = CtkBuffer((Platform.resourceDir ++ "/sounds/*").pathMatch.choose).load;

(
// EnvGen with a gate and doneAction
d = CtkSynthDef('playbuf', {
	arg buffer, gate = 1; // set the gate to 1 to hold the Env open
	var env;
	env = EnvGen.kr(Env([0, 1, 1, 0], [0.1, 1.0, 2.0], 'sin', 2), gate, doneAction: 2);
	Out.ar(0, PlayBuf.ar(1, buffer, BufRateScale.kr(buffer), loop: 1) * env)
});
)

// play the note scheduled for 5 secs in the future
x = d.note(5.0).buffer_(b).play

// set the gate to 0 and fade the synth out, then free it
x.gate_(0)

// alternately, Ctk has a built in method that will set the gate to 0, but will not free the synth without the doneAction
x.release;

// free the buffer
b.free;
::

It will probably be a lot easier to learn to schedule with Routines and Tasks. Let's take a look at low level scheduling on an actual clock, which is what Ctk does under the hood.


Note::Examples borrowed from the SuperCollider book: Clock, Routine, Task::

subsection:: SystemClock

We can use the link::Classes/SystemClock:: to schedule code to be run in the future.

code::
SystemClock.sched(2, {"foo".postln});
::

Here we have scheduled a function code::{"foo".postln}:: to be run 2 seconds in the future. If our function returns a link::Classes/Float::, then the function will be rescheduled at that time.

code::
// "foo" repeats every second
SystemClock.sched(0, {"foo".postln; 1 });

// "bar" repeats at a random delay

SystemClock.sched(0, {"bar".postln; 1.0.rand });

// clear all scheduled events
SystemClock.clear;
::

subsection:: TempoClock

link::Classes/TempoClock:: is a scheduler like SystemClock, but it schedules relative to a tempo in beats per second. Like SystemClock, we reschedule our function to be evaluated by return a value (in beats).

code::
// make a new clock and schedule someting once per beat (in this case once per second)
(
t = TempoClock.new(1); // make a new TempoClock
t.sched(0, {"Hello!".postln; 1});
)
// set the tempo to something else
t.tempo_(t.tempo * 2); // twice as fast
// get the tempo
t.tempo;
// clear the clock
t.clear;
::

subsection:: AppClock

link::Classes/AppClock:: is a similar SystemClock, but it schedules is less accurate. We need to know about AppClock because it can schedule things to happen in a GUI where as SystemClock and TempoClock can not. The easiest way is to wrap your code in a function and use the link::Classes/Function#-defer:: method.

code::
{"some GUI code".postln}.defer
::

subsection:: Routine

A link::Classes/Routine:: runs a Function and allows it to be suspended in the middle and be resumed again where it left off. A Routine is strong::started:: the first time link::Classes/Routine#-next:: is called, which will run the Function from the beginning. It is strong::suspended:: when it "yields" (using link::Classes/Object#-yield:: within the Function), and then strong::resumed:: using link::#-next:: again. When the Function returns, the Routine is considered strong::stopped::, and calling link::Classes/Routine#-next:: will have no effect  unless the Routine is strong::reset:: using link::Classes/Routine#-reset::, which will rewind the Function to the beginning. You can stop a Routine before its Function returns using link::Classes/Routine#-stop::.

code::
// Routine
(
r = Routine({
	"foo".yield;
	"bar".yield;
});
)
r.value; // foo
r.value; // bar
r.value; // we've reached the end, so it returns nil
r.next; // same thing
r.reset; // reset the routine to be evaluated again
r.next;
::

When a Routine is strong::scheduled:: on a link::Classes/Clock:: (e.g. using link::Classes/Routine#-play::), it will be started or resumed at the scheduled time. The value yielded by the Routine will be used as the time difference for rescheduling the Routine. (See
	link::Classes/Routine#-awake::).

code::
// Routine in a Clock
(
r = Routine({
	"foo".postln;
	1.yield; // reschedule after 1 second
	"bar".postln;
	1.yield;
	"foobar".postln;
});
r.play
)
::

Here we have a musical example scheduling a Routine on a clock. We can note that code::nil.yield:: with pause the routine until we resume playing it.

code::
// yield as musical fermata
s.boot;
(
d = CtkSynthDef(\default, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
	var snd, env;
	snd = SinOsc.ar(freq, 0, amp);
	env = EnvGen.kr(Env.perc, doneAction: 2);
	Out.ar(out, Pan2.ar(snd * env, pan));
});
r = Routine({
	d.note.freq_(76.midicps).play;
	0.3.wait;

	d.note.freq_(74.midicps).play;
	0.3.wait;

	d.note.freq_(73.midicps).play;
	0.3.wait;

	d.note.freq_(71.midicps).play;
	"Waiting...".postln;
	nil.yield;// fermata

	d.note.freq_(69.midicps).play;

});
)
// do this then wait for the fermata
r.play;
// finish
r.play;

//reset
r.reset //now you can play again
::

subsection:: Condition

We may find the need to pause our routine and wait for something from the server. We can use a link::Classes/Condition:: to make the routine wait for some other outside process to finish. It used to be that we had to do this with loading CtkBuffers. We have to wait until the buffer is loaded on the server before we can use it. But Marcin has fixed this in the latest release of Ctk!! Nevertheless, it is important that we know how to do this in case other situations arise.

Condition has a test variable as its first argument. This argument defaults to code::false::. While this variable remains code::false::, using the link::Classes/Condition#-wait:: method will pause the Routine inside which the Condition is created. Once, the test variable is changed to true, we can signal this to the condition and resume the routine.

code::
(
var cond, routine;

cond = Condition.new;

routine = Routine({
	var soundFile, synth, score, freqs;
	"We started the routine! And we will wait for a sound file to be rendered. So we can normalize it".postln;

	score = CtkScore.new;

	freqs = [76, 74, 73, 71, 69];

	synth = CtkSynthDef('default', {arg out = 0, freq = 440, amp = 0.5, pan = 0;
		var snd, env;
		snd = SinOsc.ar(freq, 0, amp);
		env = EnvGen.kr(Env.perc);
		Out.ar(out, Pan2.ar(snd * env, pan));
	});

	[0.0, 0.3, 0.6, 0.9, 1.2].do{arg start, inc;
		score.add(
			synth.note(start, 1.1).freq_(freqs[inc].midicps),

		);
	};

	score.write(
		path: Platform.userHomeDir ++ "/Desktop/test.wav",
		headerFormat: 'WAV',
		sampleFormat: 'int24',
		sampleRate: 48000,
		options: ServerOptions.new.numOutputBusChannels_(2),
		action: {cond.test_(true).signal}
	);

	"/*If*/ we didn't wait, we would try to normalize here".postln;

	cond.wait;

	cond.test_(false);

	soundFile = SoundFile.new;

	soundFile.openRead(Platform.userHomeDir ++ "/Desktop/test.wav");

	soundFile.normalize(Platform.userHomeDir ++ "/Desktop/test_normalize.wav");

	soundFile.close;


});

routine.play;
)
::

subsection:: Task

link::Classes/Task:: is a pauseable process. It is implemented by wrapping a link::Classes/PauseStream:: around a link::Classes/Routine::. Most of its methods (start, stop, reset) are inherited from PauseStream.

Tasks are not 100% interchangeable with Routines.

list::
## link::Classes/Condition:: does not work properly inside of a Task.
## Stopping a task and restarting it quickly may yield surprising results (see example below), but this is necessary to prevent tasks from becoming unstable if they are started and/or stopped in rapid succession.
::

Here we also see the link::Classes/Function#-loop:: method. This will infinitely repeat the function. It is similar to code::inf.do::.

code::
(
d = CtkSynthDef(\default, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
	var snd, env;
	snd = SinOsc.ar(freq, 0, amp);
	env = EnvGen.kr(Env.perc, doneAction: 2);
	Out.ar(out, Pan2.ar(snd * env, pan));
});
)
// Using Task so you can pause the sequence
(
t = Task({
	loop({	 // loop the whole thing
		3.do({	 // do this 3 times
			d.note.freq_(76.midicps).play;
			0.5.wait;
			d.note.freq_(73.midicps).play;
			0.5.wait;
		});
		"I'm waiting for you to resume".postln;
		nil.yield;// fermata
		d.note.freq_(69.midicps).play;
		1.wait;
	});
});
)
t.play;
t.resume;
t.stop;
t.reset;
::

Be carefule with loop because if you want to loop something, but also do something else within one Task or Routine, you will never get to the second thing as the loop will last until you stop.

code::
(
Task({
	loop({
		"Test1".postln;
		0.5.wait;
	});

	loop({
		"Test2".postln;
		0.5.wait;
	})

}).play
)
::

You need to have separate Tasks...

code::
(
Task({
	Task({
		loop({
			"Test1".postln;
			0.5.wait;
		});
	}).play;

	Task({
		loop({
			"Test2".postln;
			0.5.wait;
		})
	}).play

}).play
)
::


subsection:: Nesting Routines and Tasks

As we have seen above, we can nest Tasks and Routines. This allows us to have meta scheduling control where one "section" of the Routine is a Task that does many things. This will be our approach when we start using events.

code::
(
d = CtkSynthDef(\default, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
	var snd, env;
	snd = SinOsc.ar(freq, 0, amp);
	env = EnvGen.kr(Env.perc, doneAction: 2);
	Out.ar(out, Pan2.ar(snd * env, pan));
});
)
// Nesting tasks inside routines
(
r = Routine({
	c = TempoClock.new; // make a TempoClock
	// start a loop
	t = Task({
		loop({
			d.note.freq_(61.midicps).amp_(0.2).play;
			0.2.wait;
			d.note.freq_(67.midicps).amp_(0.2).play;
			rrand(0.075, 0.25).wait; // random wait from 0.1 to 0.25 seconds
		});
	}, c); // use the TempoClock to play this Task
	t.start;
	nil.yield;

	// now add some notes
	d.note.freq_(73.midicps).amp_(0.3).play;
	nil.yield;
	d.note.freq_(79.midicps).amp_(0.3).play;
	c.tempo = 2; // double time
	nil.yield;
	t.stop; // stop the Task and Synths
});
)

r.next; // start loop
r.next; // first note
r.next; // second note; loop goes 'double time'
r.next; // stop loop
::

section:: More Examples

Go through these examples in the lab.

code::
//--------
// Task examples with CtkSynthDef
(
a = CtkSynthDef(\simple, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
	var snd, env;
	snd = SinOsc.ar(freq, 0, amp);
	env = EnvGen.kr(Env.perc, doneAction: 2);
	Out.ar(out, Pan2.ar(snd * env, pan));
})
)

a.note.play;
(
t = Task({
	10.do({|i|
		a.note.amp_(-9.dbamp).play;
		0.2.wait;
	});
}).play;
)
(
t = Task({
	10.do({|i|
		a.note.freq_(rrand(440, 450)).amp_(-9.dbamp).play;
		0.2.wait;
	});
}).play
)
(
t = Task({
	10.do({|i|
		a.note.freq_(rrand(440, 1450)).amp_(-9.dbamp).play;
		0.2.wait;
	});
}).play
)
(
t = Task({
	10.do({|i|
		a.note.freq_(rrand(440, 1450)).amp_(-9.dbamp).play;
		(0.1 + 0.1.rand).wait;
	});
}).play
)
(
t = Task({
	100.do({|i|
		a.note.freq_(rrand(440, 1450)).amp_(-18.dbamp).play;
		(0.01 + 0.05.rand).wait;
	});
}).play
)
(
t = Task({
	100.do({|i|
		a.note.freq_(rrand(40, 6450)).amp_(-18.dbamp).play;
		(0.01 + 0.05.rand).wait;
	});
}).play
)
(
t = Task({
	100.do({|i|
		a.note.freq_(rrand(40 + (i * 20), 450 + (i*40))).amp_(-22.dbamp).play;
		(0.01 + 0.05.rand).wait;
	});
}).play
)
(
t = Task({
	1000.do({|i|
		a.note.freq_(rrand(40 + (i * 5), 450 + (i*10))).amp_(-28.dbamp).play;
		(0.01 + 0.02.rand).wait;
	});
}).play
)
t.stop;
t.play;
::



Now the same things in a code block, without global variables (e.g. if you would like to use it for the performance):
code::
(
var synth, task;
s.waitForBoot({
	synth = CtkSynthDef(\simple, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
		var snd, env;
		snd = SinOsc.ar(freq, 0, amp);
		env = EnvGen.kr(Env.perc, doneAction: 2);
		Out.ar(out, Pan2.ar(snd * env, pan));
	});
	task = Task({
		1000.do({|i|
			synth.note.freq_(rrand(40 + (i * 5), 450 + (i*10))).amp_(-28.dbamp).play;
			(0.01 + 0.02.rand).wait;
		});
	});
	Task({
		task.play;
		5.wait;
		task.stop;
		1.wait;
		task.play;
	}).play;
	// t = task; //this is only so we can gracetully stop if from outside the code block... it's one justified use of a global variable, but there are also other solutions to the problem of controlling things while they run - will be presented in the future;
});
)
::

subsection::Task from a Function
code::
(
var synth, task;
s.waitForBoot({
	synth = CtkSynthDef(\simple, {arg out = 0, freq = 440, amp = 0.5, pan = 0;
		var snd, env;
		snd = SinOsc.ar(freq, 0, amp);
		env = EnvGen.kr(Env.perc, doneAction: 2);
		Out.ar(out, Pan2.ar(snd * env, pan));
	});
	task = {|iterations = 1000, minWaitTime = 0.01|
		Task({
			iterations.do({|i|
				synth.note.freq_(rrand(40 + (i * 5), 450 + (i*10))).amp_(-28.dbamp).play;
				(minWaitTime + 0.02.rand).wait;
			});
		});
	};
	Task({
		var taskInstance; //local variable inside this function (which is inside this task)
		task.value(100, 0.12).play;
		5.wait;
		taskInstance = task.value(1000, 0.01).play;
		1.wait;
		taskInstance.stop;
		2.wait;
		taskInstance.play;
	}).play;
	// task.value(10, 0.2).play;
	// t = task; //this is only so we can gracetully stop if from outside the code block... it's one justified use of a global variable, but there are also other solutions to the problem of controlling things while they run - will be presented in the future;
});
)

::

subsection:: CtkScore Example 1
code::
//Converting score-based (non-realtime - NRT) examples to realtime for live performance etc.

//first, review how tasks and routines work
"Routines and Tasks".help; //choose "15. Sequencing with Routines and Tasks"

// ------------------------------------------
// ---------------- 1 - score -----------------
// ------------------------------------------

//for conversion - a simple score example
s.boot;
(
var score, synth, options;
// create a CtkScore to fill with notes
score = CtkScore.new;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});
score.add(synth.note(0.2, 2.0).freq_(420).envDur_(2).amp_(0.5)); //first start time is 0.2 to allow for loading the synthdef
score.add(synth.note(1.0, 2.0).freq_(440).envDur_(2).amp_(0.5));
score.add(synth.note(3.0, 2.0).freq_(440).envDur_(2).amp_(0.2));
score.add(synth.note(3.1, 2.0).freq_(548).envDur_(2).amp_(0.2));
score.add(synth.note(3.2, 2.0).freq_(332).envDur_(2).amp_(0.2));
score.add(synth.note(3.3, 2.0).freq_(656).envDur_(2).amp_(0.2));
score.add(synth.note(3.4, 2.0).freq_(424).envDur_(2).amp_(0.2));
score.add(synth.note(3.5, 2.0).freq_(564).envDur_(2).amp_(0.2));
score.add(synth.note(3.6, 2.0).freq_(816).envDur_(2).amp_(0.2));
score.add(synth.note(5.1, 2.0).freq_(412).envDur_(2).amp_(0.2));
score.play;
)
::
subsection::Realtime Example 1


Now, to convert it to realtime, we need to:
list::
##schdule notes using a Task
##.wait (or .yield) between events/notes; we need to provide time differences, as opposed to absolute times since the start of the piece
##play notes right away (start time = 0 and ".play" it)
::
code::
s.reboot;
(
var task, synth, options;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});
task = Task({
	0.2.wait; //wait for 0.2 seconds; same as 0.2.yield
	synth.note(0, 2.0).freq_(420).envDur_(2).amp_(0.5).play; //play the note right away
	0.8.wait; //wait
	synth.note(0, 2.0).freq_(440).envDur_(2).amp_(0.5).play; //play etc.
	2.wait;
	synth.note(0, 2.0).freq_(440).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(548).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(332).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(656).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(424).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(564).envDur_(2).amp_(0.2).play;
	0.1.wait;
	synth.note(0, 2.0).freq_(816).envDur_(2).amp_(0.2).play;
	1.5.wait;
	synth.note(0, 2.0).freq_(412).envDur_(2).amp_(0.2).play;
});

task.play; //play the task
)

::

subsection:: CtkScore Example 2
When you use functions to generate notes, it makes it even easier to conert code from NRT to realtime

code::
//score example
(
var score, synth, options, startTimes, freqs, amps;
// create a CtkScore to fill with notes
score = CtkScore.new;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});
startTimes = [0.2, 1.0, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 5.1];
freqs = [420, 440, 440, 548, 332, 656, 424, 564, 816, 412];
amps = [0.5, 0.5, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2];

startTimes.size.do({arg inc;
	score.add(synth.note(startTimes[inc], 2.0).freq_(freqs[inc]).envDur_(2).amp_(amps[inc]));
});
score.play;
)

::
subsection:: Realtime Example 2
Now converted to realtime. If needed, conversion to relative times can be done programmatically (but often times you will  already have that data, see below)
code::
(
var task, synth, options, startTimes, startTimesRelative, freqs, amps;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});

startTimes = [0.2, 1.0, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 5.1];
freqs = [420, 440, 440, 548, 332, 656, 424, 564, 816, 412];
amps = [0.5, 0.5, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2];

startTimesRelative = startTimes.collect({arg thisStartTime, inc;
	var thisRelativeStartTime, previousStartTime;
	previousStartTime = startTimes[inc-1];
	if(previousStartTime.notNil, {//first check if we can get precious start time
		thisRelativeStartTime = thisStartTime - previousStartTime; //if so, subtract previous from current
	}, {
		thisRelativeStartTime = thisStartTime; //if not, treat it as the first start time, don's subtract anythin
	});
	thisRelativeStartTime; //important - return thisRelativeStartTime, so .collect will return an array of these
});
"startTimesRelative: ".post; startTimesRelative.postln;

task = Task({
	startTimesRelative.size.do({arg inc;
		"waiting ".post; startTimesRelative[inc].post; "s".postln; //since we're in realtime, we can post things as they happen
		startTimesRelative[inc].wait; //wait
		"playing sound at frequency ".post; freqs[inc].post; "Hz".postln;
		synth.note(0, 2.0).freq_(freqs[inc]).envDur_(2).amp_(amps[inc]).play; //startTime = 0, .play right away
	});
});
task.play; //play the task
)
::

subsection:: CtkScore Example 3


Often we think about time in relative values between items, for example:
code::
(
var score, synth, options, times, freqs, amps, now;
// create a CtkScore to fill with notes
score = CtkScore.new;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});
times = [ 0.2, 0.8, 2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 1.5 ]; //relative times
freqs = [420, 440, 440, 548, 332, 656, 424, 564, 816, 412];
amps = [0.5, 0.5, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2];

now = 0; //initialize variable to keep the time information

times.size.do({arg inc;
	now = now + times[inc]; //increment time by the relative time value
	"now: ".post; now.postln;
	score.add(synth.note(now, 2.0).freq_(freqs[inc]).envDur_(2).amp_(amps[inc])); //use now as startTime
});
score.play;
)
::

subsection:: Realtime Example 3


And then coverting to realtime is even easier.
code::
(
var task, synth, options, times, freqs, amps;

synth = CtkSynthDef(\oscili,  {arg freq, amp, envDur;
	var osc, osc2, env;
	osc = SinOsc.ar(freq, 0, amp);
	env = Line.kr(1, 0, envDur, doneAction: 0);
	Out.ar(0, osc * env)
});

times = [ 0.2, 0.8, 2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 1.5 ]; //relative times
freqs = [420, 440, 440, 548, 332, 656, 424, 564, 816, 412];
amps = [0.5, 0.5, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2];

//no need to have incremented "now" variable

task = Task({
	times.size.do({arg inc;
		"current time diff: ".post; times[inc].postln;
		times[inc].wait; //wait for a specified time
		synth.note(0, 2.0).freq_(freqs[inc]).envDur_(2).amp_(amps[inc]).play; //0 as startTime, and .play it
	});
});
task.play;//play the dask
)

::

section::Credits

Marcin Pączkowski, University of Washington, 2014-2018

Daniel Peterson, University of Washington, 2018
