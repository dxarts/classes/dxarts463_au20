title:: VSTPlugin
summary:: DXARTS 463
categories:: Tutorials>DXARTS>463
keyword:: DXARTS

table::
## strong::TOC:: || link::Tutorials/DXARTS-463-TOC::
## strong::Previous:: || link::Tutorials/Algorithms-II::
## strong::Next:: || link::Tutorials/Vocoder::
::

section:: VST
Recently VST plugins were made available to use natively in SuperCollider, using the VSTPlugin project. The tools can be downloaded here link::https://git.iem.at/pd/vstplugin/-/releases::. The tools can be used directly, see link::Classes/VSTPlugin:: and link::Classes/VSTPluginController:: for more examples.

section:: HoaVST

Most VST plugins are either mono or stereo. When using these with the ATK in first order, we need 4 channels of audio. This means that we need either 2 stereo plugins or 4 mono plugins. If we are also not wanting to affect the soundfield spatailly, we need these (2 or 4) plugins to have the exact same parameters. In Reaper we can link  plugin parameters (please let me know if you would like to know how to do this) one by one. If a plugin has many parameters this can be very tedious and also easily lead to mistakes. Using the plugins in SuperCollider, this becomes trivial. So, I have written my own custom wrapper for VSTPlugin to allow for use with the ATK called link::Classes/HoaVST::.

code::
Quarks.install("https://gitlab.com/dxarts/projects/hoavst.quark")
::

subsection:: RT Stereo Example

code::
CtkObj.latency_(nil);
(
s.waitForBoot({

	~touchOSC = NetAddr("10.0.0.143", 9000);

	~vst = HoaVST.new('ValhallaFreqEcho', vstChannels: 2, automateParams: true);

	~buffer = CtkBuffer("/Users/dan/Desktop/Desktop/DX460/DX460_Sounds/Bells1.wav").load;

	s.sync;

	~synth = CtkSynthDef('vsttest', { |buffer|
		var in, vst;
		in = PlayBuf.ar(2, buffer, BufRateScale.kr(buffer), loop: 1);
		// run the VSTPlugin
		vst = ~vst.ar(in);
		Out.ar(0, vst)
	});

	s.sync;

	~note = ~synth.note.buffer_(~buffer).play;

	s.sync;

	// launch the controller
	~vst.pluginController(~note, ~synth);

	// open the vst
	~vst.open;

	// control via TouchOSC
	~oscFuncs = ~vst.parameters.collect{ |paramName, inc|
		var path;
		path = ('/2/fader' ++ (inc + 1)).asSymbol;
		OSCFunc({ |msg|
			~vst.perform(paramName.asSetter, msg[1])
		}, path, ~touchOSC)
	};
})
)

// get the list of parameters that we can set
~vst.parameters;

// open the gui
~vst.gui;
// or the editor
~vst.editor

// set the wetDry parameter
~vst.wetDry_(0.75);
~vst.feedback_(0.59);

// set the wetDry to a CtkContol
~vst.wetDry_(CtkControl.lfo(LFNoise2, 0.2, 0.0, 1.0))

// start the updater so we can see the CtkControl in the GUI
~vst.guiUpdater

// free the osc
~oscFuncs.do(_.free)


::


subsection:: ATK Example

code::
(
s.waitForBoot({

	~tDesign = TDesign.newHoa(24);

	~decoder = FoaDecoderKernel.newListen;

	// automate only wetdry
	~vst = HoaVST.new('ValhallaFreqEcho', numVSTs: 12, vstChannels: 2, tDesign: ~tDesign, automateParams: ['wetDry']);

	~buffer = CtkBuffer("/Users/dan/Projects/Sound_Library/EigenAtk/OctopusNFDist/Octopus_RhythmHigh1_1.5.wav").load;

	s.sync;

	~synth = CtkSynthDef('vsttest', { |buffer, amp = 0.1, theta = 0.0|
		var in, vst;
		in = PlayBuf.ar(16, buffer, BufRateScale.kr(buffer), loop: 1);
		in = HoaRotate.ar(in, theta);
		// run the plugins on aformat
		vst = ~vst.ar(in);

		// decode to FOA
		vst = HoaNFProx.ar(HPF.ar(vst, 20));
		vst = HoaDecodeMatrix.ar(vst[0..3], HoaMatrixDecoder.newFormat('fuma', 1));

		// decode to stereo
		Out.ar(0, FoaDecode.ar(vst, ~decoder))

	});
	s.sync;

	~note = ~synth.note.buffer_(~buffer).play;

	s.sync;

	// load the controllers
	~vst.pluginController(~note, ~synth);

	// opne the vst
	~vst.open;

	// control via TouchOSC
	~oscFuncs = ~vst.parameters.collect{ |paramName, inc|
		var path;
		path = ('/2/fader' ++ (inc + 1)).asSymbol;
		OSCFunc({ |msg|
			~vst.perform(paramName.asSetter, msg[1])
		}, path, ~touchOSC)
	};
})
)

// get the list of parameters that we can set
~vst.parameters;

// opne the gui
~vst.gui;

// set parameters
~vst.wetdry_(1.0);
~vst.wetdry_(CtkControl.lfo(LFNoise2, 0.2, 0.0, 1.0));
~vst.shift_(0.73)
~vst.feedback_(0.65);
~vst.lowcut_(0.0);
~vst.highcut_(1.0);
~vst.stereo_(1);
~vst.sync_(0);
~vst.delay_(0.7);

// start the gui updater
~vst.guiUpdater

// open the editor
~vst.editor

// get a paramter that has been automated
~wetDry = ~vst.wetdry

// if parameter is not automated, need to supply function
~vst.feedback({ |val| val.postln })

~vst.synthParams

~vst.automateParams
::


subsection:: NRT Example

code::

(
~score = CtkScore.new;

~tDesign = TDesign.newHoa(24);

// must set automateParams to true for NRT
~vst = HoaVST.new('ValhallaFreqEcho', numVSTs: 12, vstChannels: 2, tDesign: ~tDesign, automateParams: true);

~buffer = CtkBuffer("/Users/dan/Projects/Sound_Library/EigenAtk/OctopusNFDist/Octopus_RhythmHigh1_1.5.wav").addTo(~score);

~synth = CtkSynthDef('vsttest', { |buffer, amp = 0.1, theta = 0.0|
    var in, vst;
    in = PlayBuf.ar(16, buffer, BufRateScale.kr(buffer), loop: 1);
    in = HoaRotate.ar(in, theta);
    // run the plugins on aformat
    vst = ~vst.ar(in);

    // decode to FOA
    vst = HoaNFProx.ar(HPF.ar(vst, 20));
    vst = HoaDecodeMatrix.ar(vst[0..3], HoaMatrixDecoder.newFormat('fuma', 1));

    // decode to stereo
    Out.ar(0, FoaDecode.ar(vst, FoaDecoderMatrix.newStereo))

});

~note = ~synth.note(0.0, 60.0).buffer_(~buffer).addTo(~score);

// load the controllers
~vst.pluginController(~note, ~synth);

// opne the vst
~vst.openMsg.do(_.addTo(~score));


// set parameters
~vst.wetdry_(CtkControl.lfo(LFNoise2, 0.2, 0.4, 1.0));
~vst.shift_(CtkControl.lfo(LFNoise2, 0.2, 0.3, 0.4));
~vst.feedback_(0.75);
~vst.lowcut_(0.0);
~vst.highcut_(1.0);
~vst.stereo_(1);
~vst.sync_(0);
~vst.delay_(CtkControl.lfo(LFNoise2, 0.2, 0.3, 0.5));

~score.write(
    path: "~/Desktop/vstTest.wav".standardizePath,
    sampleRate: 48000,
    headerFormat: 'WAV',
    sampleFormat: 'float',
    options: ServerOptions.new.numOutputBusChannels_(2)
)
)

::
section:: Credits

Daniel Peterson, University of Washington, 2020