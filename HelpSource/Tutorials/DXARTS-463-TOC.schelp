title:: DXARTS 463: Advanced Digital Sound Synthesis and Processing
summary:: DXARTS 463 - TOC
categories:: Tutorials>DXARTS>463
keyword:: DXARTS

table::
## strong::Next:: ||link::Tutorials/SuperCollider-Review::
::

section:: Synopsis

Advanced sound processing and synthesis techniques. Includes sound time warping; analysis-synthesis techniques; linear predictive coding; the phase vocoder; frequency-domain sound transformations; introduction to physical modeling

link::https://uwstudent.washington.edu/student/myplan/course/DXARTS463##View course details in MyPlan: DXARTS 463::

Offered by the link::https://dxarts.washington.edu/about##Center for Digital Arts and Experimental Media (DXARTS):: at the link::http://www.washington.edu/##University of Washington::.

section:: Table of Contents

definitionList::
## Week 0 - Orientation & Review ||
list::
## link::Tutorials/SuperCollider-Installation::
## link::Tutorials/SuperCollider-Review::
::
## Week 1 - Schedulers ||
list::
## link::Tutorials/Scheduling::
## link::Tutorials/Real-Time-Sound::
::
## Week 2 - Events and Controllers ||
list::
## link::Tutorials/Events::
## link::Tutorials/MIDI-OSC::
## link::Tutorials/OSC-Reaper::
::
## Week 3 - MIDI Data ||
list::
## link::Tutorials/MIDIData::
::
## Week 4 - SoundMap ||
list::
## link::Tutorials/SoundMap::
::
## Week 5 - GUI - Classes ||
list::
## link::Tutorials/GUI::
## link::Tutorials/Classes::
::
## Week 6 - ATK ||
list::
## link::Tutorials/ATK::
## link::Tutorials/ATK-II::
::
## Week 7 - Algorithmic Composition and VSTs ||
list::
## link::Tutorials/Algorithms::
## link::Tutorials/Algorithms-II::
## link::Tutorials/VST::
::
## Week 9 - Vocoder and Freesound ||
list::
## link::Tutorials/Vocoder::
## link::Tutorials/Freesound::
::
::
